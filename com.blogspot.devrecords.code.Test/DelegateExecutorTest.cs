﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using com.blogspot.devrecords.code.examples;
using System.Diagnostics;

namespace com.blogspot.devrecords.code.Test
{
    [TestClass]
    public class DelegateExecutorTest
    {
        delegate void TestDelegate();
        
        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void ExecuteTest()
        {
            var d = new TestDelegate(Method) + MethodWithException + Method1;
            DelegateExecutor.Execute(d);
        }

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void ExecuteWithInvokerTest()
        {
            var d = new TestDelegate(Method) + MethodWithException + Method1;
            DelegateExecutor.Execute(d, new Action<Delegate>(Invoker));
        }

        void Invoker(Delegate d)
        {
            var del = d as TestDelegate;
            if (del != null)
                del();
        }

        private void Method()
        {
            Debug.WriteLine("Call Method");
        }

        private void Method1()
        {
            Debug.WriteLine("Call Method1");
        }

        private void MethodWithException()
        {
            Debug.WriteLine("Call MethodWithException");
            throw new Exception("Something goes wrong!");
        }

        const int loopCount = 5000000;
        [TestMethod]
        public void ExecutePerformanceTest()
        {
            var d = new TestDelegate(DoNothing);
            var watch = Stopwatch.StartNew();
            for (int i = 0; i < loopCount; i++)
                DelegateExecutor.Execute(d);
            watch.Stop();
            Debug.WriteLine("ExecutePerformanceTest took " + watch.Elapsed);
        }

        [TestMethod]
        public void ExecuteWithInvokerPerformanceTest()
        {
            var d = new TestDelegate(DoNothing);
            var watch = Stopwatch.StartNew();
            for (int i = 0; i < loopCount; i++)
                DelegateExecutor.Execute(d, new Action<Delegate>(Invoker));
            watch.Stop();
            Debug.WriteLine("ExecuteWithInvokerPerformanceTest took " + watch.Elapsed);
        }

        [TestMethod]
        public void ExecuteUsingInvokePerformanceTest()
        {
            var d = new TestDelegate(DoNothing);
            var watch = Stopwatch.StartNew();
            for (int i = 0; i < loopCount; i++)
                d();
            watch.Stop();
            Debug.WriteLine("ExecuteUsingInvokePerformanceTest took " + watch.Elapsed);
        }

        static void DoNothing()
        {
        }

        event EventHandler<EventArgs> TestEvent;

        [TestMethod]
        [ExpectedException(typeof(AggregateException))]
        public void ExecuteTestWithArgs()
        {
            TestEvent = new EventHandler<EventArgs>(Method) + MethodWithException + Method1;
            DelegateExecutor.Execute(TestEvent, this, EventArgs.Empty);
        }

        void Method(object sender, EventArgs e)
        {
            Debug.WriteLine("Call Method");
        }

        void Method1(object sender, EventArgs e)
        {
            Debug.WriteLine("Call Method1");
        }

        void MethodWithException(object sender, EventArgs e)
        {
            Debug.WriteLine("Call MethodWithException");
            throw new Exception("Something goes wrong!");
        }
    }
}
