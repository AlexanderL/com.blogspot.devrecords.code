﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace com.blogspot.devrecords.code.examples
{
    public static class DelegateExecutor
    {
        public static void Execute(Delegate del, object sender, EventArgs e)
        {
            var exList = new List<Exception>();
            foreach (Delegate d in del.GetInvocationList())
            {
                try
                {
                    if(sender==null)
                        d.DynamicInvoke(null);
                    else
                        d.DynamicInvoke(new object[] { sender, e });
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    exList.Add(ex);
                }
            }
            if (exList.Count > 0)
                throw new AggregateException(exList);
        }

        public static void Execute(Delegate del)
        {
            Execute(del, null, null);
        }

        public static void Execute(Delegate del, Action<Delegate> invoker)
        {
            var exList = new List<Exception>();
            foreach (Delegate d in del.GetInvocationList())
            {
                try
                {
                    invoker(d);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    exList.Add(ex);
                }
            }
            if (exList.Count > 0)
                throw new AggregateException(exList);
        }
    }
}
